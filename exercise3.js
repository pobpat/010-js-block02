
var arr = ["banana", "apple", "orange"];
var arr2 = ["tv", "dvd-player", "playstation"];
var arr3 = [];
var swaperOne;
var swaperTwo;
var pos = 2;
function swap(arg1, arg2, pos) {
    swaperOne = arg1[pos];
    swaperTwo = arg2[pos];
    arg1[pos] = swaperTwo;
    arg2[pos] = swaperOne;
    arr3.push(arg1);
    arr3.push(arg2);
    return arr3;
};

module.exports = { swap }