var arr = ["car", "soap", "banana", "tv", "toothbrush"];
function removeFirstAndLast(arg) {
    arg.shift();
    arg.pop();
    return arg;
}
module.exports = {
    removeFirstAndLast
}